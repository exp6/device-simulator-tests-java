package cl.metricarts.iotlab.fleetsensor;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.microsoft.azure.eventhubs.EventData;
import com.microsoft.azure.eventhubs.EventHubClient;
import com.microsoft.azure.servicebus.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.ServiceBusException;

/**
 * Azure Event Hub example 
 *
 */
public class App 
{
    public static void main( String[] args )
    { 
    	final String namespaceName = "metricpb-eventhub";
        final String eventHubName = "infromsubscriberhub";
        final String sasKeyName = "infromsubscriberhub";
        final String sasKey = "j6y/fqRxnNOhWum2pKucz2sm5imXi0d/U5f/wpDJumU=";
        ConnectionStringBuilder connStr = new ConnectionStringBuilder(namespaceName, eventHubName, sasKeyName, sasKey);
    	
		try {
			EventHubClient ehClient = EventHubClient.createFromConnectionStringSync(connStr.toString());
			ehClient = EventHubClient.createFromConnectionStringSync(connStr.toString());
		     
			JSONArray values = new JSONArray();
			values.add("-33.446886");
			values.add("-70.646138");
			
			JSONObject data = new JSONObject();         
			data.put("latlng", values);
			data.put("tipo", "BUS");
			data.put("patente", "XXYY12");
			
			JSONObject data1 = new JSONObject();
			data1.put("data", data);
			
			byte[] payloadBytes = data1.toString().getBytes("UTF-8");
	        EventData sendEvent = new EventData(payloadBytes);
	        ehClient.send(sendEvent);
	        
	        System.out.println(data);
			
		}
		catch (ServiceBusException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
