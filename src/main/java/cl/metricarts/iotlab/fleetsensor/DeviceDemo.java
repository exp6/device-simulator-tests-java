package cl.metricarts.iotlab.fleetsensor;

import com.microsoft.azure.sdk.iot.device.DeviceClient;
import com.microsoft.azure.sdk.iot.device.IotHubClientProtocol;
import com.microsoft.azure.sdk.iot.device.Message;
import com.microsoft.azure.sdk.iot.device.IotHubStatusCode;
import com.microsoft.azure.sdk.iot.device.IotHubEventCallback;
import com.microsoft.azure.sdk.iot.device.MessageCallback;
import com.microsoft.azure.sdk.iot.device.IotHubMessageResult;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class DeviceDemo {

	private static final String connectionString = "HostName=iot-lab-hub.azure-devices.net;DeviceId=Fleet001;SharedAccessKey=y8AWRZVAGt+eev6tNvm3DQ4TlAOh1kKI5GXnHbQUNCM=";
	private static IotHubClientProtocol protocol = IotHubClientProtocol.MQTT;
	private static final String deviceId = "Fleet001";
	private static DeviceClient client;
	
	public static void main(String[] args) throws IOException, IllegalArgumentException, URISyntaxException {
		client = new DeviceClient(connectionString, protocol);
		  client.open();

		  MessageSender sender = new MessageSender();

		  ExecutorService executor = Executors.newFixedThreadPool(1);
		  executor.execute(sender);

		  System.out.println("Press ENTER to exit.");
		  System.in.read();
		  executor.shutdownNow();
		  client.close();
	}
	
	private static class EventCallback implements IotHubEventCallback
	{
	  public void execute(IotHubStatusCode status, Object context) {
	    System.out.println("IoT Hub responded to message with status: " + status.name());

	    if (context != null) {
	      synchronized (context) {
	        context.notify();
	      }
	    }
	  }
	}
	
	private static class TelemetryDataPointSub {
		  public String[] latlng;
		  public String tipo;
		  public String patente;

		  public String serialize() {
		    Gson gson = new Gson();
		    return gson.toJson(this);
		  }
		}

	private static class TelemetryDataPoint {
		  public String deviceId;
		  public TelemetryDataPointSub data;

		  public String serialize() {
		    Gson gson = new Gson();
		    return gson.toJson(this);
		  }
		}
	
	private static class MessageSender implements Runnable {

		  public void run()  {
			  
			  String[] lats = {"-33.446659","-33.446286","-33.446095","-33.445211","-33.444855"};
			  String[] lngs = {"-70.647021","-70.645273","-70.642555","-70.640335","-70.638431"};
			  
			  int indexes = 0;
			  int totalLength = lats.length;
			  
			  /**
			   * -33.446659, -70.647021
			   * -33.446286, -70.645273
			   * -33.446095, -70.642555
			   * -33.445211, -70.640335
			   * -33.444855, -70.638431
			   * 
			   */
			  
		    try {
		      
		      while (true) {
		        
		    	if(indexes == totalLength) indexes = 0; 
		    	  
		        TelemetryDataPoint telemetryDataPoint = new TelemetryDataPoint();
		        telemetryDataPoint.deviceId = deviceId;
		        telemetryDataPoint.data = new TelemetryDataPointSub();
		        telemetryDataPoint.data.latlng = new String[2];
//		        telemetryDataPoint.data.latlng[0] = "-33.446886";
//		        telemetryDataPoint.data.latlng[1] = "-70.646138";
		        telemetryDataPoint.data.latlng[0] = lats[indexes];
		        telemetryDataPoint.data.latlng[1] = lngs[indexes];
		        telemetryDataPoint.data.tipo = "AUTO";
		        telemetryDataPoint.data.patente = "XXYY12";

		        ++indexes;
		        
		        String msgStr = telemetryDataPoint.serialize();
		        Message msg = new Message(msgStr);
		        msg.setProperty("temperatureAlert", "false");
		        msg.setMessageId(java.util.UUID.randomUUID().toString()); 
		        System.out.println("Sending: " + msgStr);

		        Object lockobj = new Object();
		        EventCallback callback = new EventCallback();
		        client.sendEventAsync(msg, callback, lockobj);

		        synchronized (lockobj) {
		          lockobj.wait();
		        }
		        Thread.sleep(5000);
		      }
		    } catch (InterruptedException e) {
		      System.out.println("Finished.");
		    }
		  }
		}
}
